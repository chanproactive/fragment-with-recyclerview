package com.chan.recyclerview4

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chan.recyclerview4.adapter.PhotoAdapterItem
import com.chan.recyclerview4.listener.OnItemClickListener
import com.chan.recyclerview4.model.UserModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException

class PhotoActivity : AppCompatActivity(), OnItemClickListener {
    override fun onCheckChange(position: Int) {
//        if (dataList[position].isChecked){
//            dataList[position].isChecked = false}
//        else{
//            dataList[position].isChecked = true}

    }

    override fun test(position: Int) {
        Toast.makeText(applicationContext, "position ${position + 1} is selected", Toast.LENGTH_SHORT).show()

    }

    override fun onClicked(position: Int) {

        var userImfo = dataList[position]
        val i = Intent(this, UserProfile::class.java)
        i.putExtra("userInfo", userImfo)
        startActivity(i)


    }

    private lateinit var dataList: ArrayList<UserModel>
    lateinit var layoutManager: RecyclerView.LayoutManager
    lateinit var adapter: PhotoAdapterItem


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        testData()
        getData()

        setLayoutManager()
        setupRecyclerView()

    }

    private fun getData() {
        val endpoint = "https://jsonplaceholder.typicode.com/users"
        val client = OkHttpClient()
        val request = Request.Builder()
            .url(endpoint)
            .build()
//        val response: Response =client.newCall(request).execute()


        val call = client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    response.body?.let {
                        val data = it.string()
                        mapDataToJson(data)
                        runOnUiThread {
                            setAdapter()
                        }
                    }
                }

            }

        })
    }

    fun setLayoutManager() {
        layoutManager = LinearLayoutManager(this)

    }

    fun setupRecyclerView() {
        recyclerView_main.layoutManager = layoutManager
        recyclerView_main.setHasFixedSize(true)
    }

    private fun setAdapter() {
        adapter = PhotoAdapterItem()
        adapter.listener = this@PhotoActivity
        adapter.dataList = dataList
        recyclerView_main.adapter = adapter
    }


    fun mapDataToJson(data: String) {
        val gson = Gson()
        val collectionType = object : TypeToken<List<UserModel>>() {}.type

        dataList = gson.fromJson(data, collectionType) as ArrayList<UserModel>

        //  Log.d("MainActivity","commentS Dada =${model[1].body}")

        //  Log.d("MainActivity","commentS Dada =${model[1].body}")


    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList("dataList", dataList)


    }

}
