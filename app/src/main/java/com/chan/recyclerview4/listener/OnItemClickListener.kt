package com.chan.recyclerview4.listener

interface OnItemClickListener {
    fun onClicked(position: Int)
    fun test(position: Int)
    fun onCheckChange(position: Int)

}