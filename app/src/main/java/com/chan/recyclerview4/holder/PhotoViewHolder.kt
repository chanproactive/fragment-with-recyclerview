package com.chan.recyclerview4.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.chan.recyclerview4.listener.OnItemClickListener
import com.chan.recyclerview4.model.UserModel
import kotlinx.android.synthetic.main.photo_item_show.view.*

class PhotoViewHolder(var view: View) : RecyclerView.ViewHolder(view) {


    fun bindViewHolder(
        position: Int,
        listener: OnItemClickListener,
        dataList: ArrayList<UserModel>
    ) {
        view.rootView.setOnClickListener {
            listener.onClicked(position)
            listener.test(position)
        }



        view.textViewName.text = dataList[position].name
        view.textViewEmail.text = dataList[position].email
        view.textViewTel.text = dataList[position].phone

//        Glide.with(view.context)
//            .load(dataList[position].thumbnailUrl)
//            .into(view.imageView)
    }


}