package com.chan.recyclerview4.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chan.recyclerview4.R
import com.chan.recyclerview4.holder.PhotoViewHolder
import com.chan.recyclerview4.listener.OnItemClickListener
import com.chan.recyclerview4.model.UserModel

class PhotoAdapterItem : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    public lateinit var listener: OnItemClickListener

    //   private val nameInTheBox = listOf("Chan","Jane","Awl","Ton")
    lateinit var dataList: ArrayList<UserModel>

    override fun getItemCount(): Int = dataList.size

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.photo_item_show, parent, false)
        return PhotoViewHolder(view)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is PhotoViewHolder -> {
                holder.bindViewHolder(position, listener, dataList)

            }
        }

    }

}