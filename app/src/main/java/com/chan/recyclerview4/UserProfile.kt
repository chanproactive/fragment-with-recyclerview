package com.chan.recyclerview4

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.chan.recyclerview4.model.UserModel
import kotlinx.android.synthetic.main.activity_user_profile.*

class UserProfile : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        val userInfo = intent.getParcelableExtra<UserModel>("userInfo")

        var street = userInfo.address!!.street.toString()
        var suit = userInfo.address!!.suit.toString()
        var city = userInfo.address!!.city.toString()
        var zipcode = userInfo.address!!.zipcode.toString()


        textFirstName.text = userInfo.name!!.toString()
        textLastName.text = userInfo.email!!.toString()
        textBirthday.text = userInfo.phone!!.toString()
        textGender.text = userInfo.website!!.toString()
        textEmail.text = userInfo.company!!.name.toString()
        textLocation.text = "$street$suit$city$zipcode"
        textView3.text = userInfo.name!!.toString()
    }
}
